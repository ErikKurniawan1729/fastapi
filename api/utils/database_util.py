import databases
import sqlalchemy
from functools import lru_cache
from api.config import config
from api.models.models import metadata


# 1. Using Pydantic to load .env configuration file
@lru_cache()
def setting():
    return config.Settings()


def database_mysql_url_config():
    return str(
        setting().DB_CONNECTION + "://"
        + setting().DB_USERNAME + ":"
        + setting().DB_PASSWORD + "@"
        + setting().DB_HOST + ":"
        + setting().DB_PORT + "/"
        + setting().DB_DATABASE
    )


database = databases.Database(database_mysql_url_config())
engine = sqlalchemy.create_engine(database_mysql_url_config())

# Migrations table from models (ORM)
metadata.create_all(engine)

import jwt
from api.utils import constant_util
from datetime import datetime, timedelta
from jose import jwt
from fastapi import Depends, HTTPException, status
from fastapi.security import OAuth2PasswordBearer
from jose import JWTError
from pydantic import ValidationError
from jwt import PyJWTError
from api.schemas import auth_schema as schema
from api.crud import auth_crud as crud

async def create_access_token(*, data: dict, expires_delta: timedelta = None):
    to_encode = data.copy()
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(minutes=constant_util.ACCESS_TOKEN_EXPIRE_MINUTE)
    to_encode.update({"exp": expire})
    return jwt.encode(to_encode, constant_util.SECRET_KEY, algorithm=constant_util.ALGORITHM)

oauth2_scheme = OAuth2PasswordBearer(
    tokenUrl="/api/auth/login"
)


def get_token_user(token: str = Depends(oauth2_scheme)):
    return token


async def get_current_user(token: str = Depends(oauth2_scheme)):
    credentials_exception = HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED, 
        detail="Could not validate credentials", 
        headers={"WWW-Authenticate": "Bearer"}
    )

    try:
        payload = jwt.decode(token, constant_util.SECRET_KEY, algorithms=[constant_util.ALGORITHM])
        username: str = payload.get("sub")
        if username is None:
            raise credentials_exception
        
        # check blacklist token
        black_list_token = await crud.find_black_list_token(token)
        if black_list_token:
            raise credentials_exception

        # check user existed
        result = await crud.find_exist_user(username)
        if not result:
            raise HTTPException(status_code=404, detail="User not found.")

        return schema.UserList(**result)

    except (PyJWTError, ValidationError, JWTError):
        raise credentials_exception

def get_current_active_user(current_user: schema.UserList = Depends(get_current_user)):
    if current_user.is_active != '1':
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Inactive user")
    
    return current_user
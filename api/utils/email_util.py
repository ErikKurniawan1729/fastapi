from fastapi_mail import ConnectionConfig, FastMail, MessageSchema, MessageType
from typing import List
from functools import lru_cache
from api.config import config


@lru_cache()
def setting():
    return config.Settings()


conf = ConnectionConfig(
    MAIL_USERNAME = setting().MAIL_USERNAME,
    MAIL_PASSWORD = setting().MAIL_PASSWORD,
    MAIL_FROM = setting().MAIL_FROM,
    MAIL_PORT = setting().MAIL_PORT,
    MAIL_SERVER = setting().MAIL_SERVER,
    MAIL_STARTTLS = setting().MAIL_STARTTLS,
    MAIL_SSL_TLS = setting().MAIL_SSL_TLS,
    USE_CREDENTIALS = setting().USE_CREDENTIALS,
    VALIDATE_CERTS = setting().VALIDATE_CERTS
)


async def send_email(subject: str, recipient: List, message: str):
    message = MessageSchema(
        subject=subject, 
        recipients=recipient, 
        body=message, 
        subtype="html"
    )
    fm = FastMail(conf)
    await fm.send_message(message)
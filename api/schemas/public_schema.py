from pydantic import BaseModel
from sqlalchemy import TIMESTAMP


class EmailCheck(BaseModel):
    email: str

class EmailCheckResponse(EmailCheck):
    id: int
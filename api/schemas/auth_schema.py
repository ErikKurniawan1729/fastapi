from pydantic import BaseModel
from typing import Optional
from datetime import datetime



class UserList(BaseModel):
    id: int = None
    username: str
    email: str
    telp: str
    is_active: str = None
    created_at: Optional[datetime] = None
    updated_at: Optional[datetime] = None


class UserCreate(UserList):
    password: str
    confirm_password: Optional[str] = None

class ForgotPassword(BaseModel):
    email: str

class ResetPassword(BaseModel):
    reset_password_token: str
    new_password: str
    confirm_password: str

class ResponseRegister(BaseModel):
    username: str
    email: str
    telp: str
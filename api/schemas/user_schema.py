from pydantic import BaseModel, Field
from typing import Optional
from datetime import date
from sqlalchemy import TIMESTAMP

class UserUpdate(BaseModel):
    nik: Optional[str] = None
    nama_lengkap: Optional[str] = None
    email: Optional[str] = None
    gender: Optional[str] = None
    birth_place: Optional[str] = None
    birth_date: Optional[date] = None
    job: Optional[str] = None
    job_location: Optional[str] = None
    province: Optional[str] = None
    city: Optional[str] = None
    district: Optional[str] = None
    postal_code: Optional[str] = None
    address: Optional[str] = None

class UserUpdateTimestamp(UserUpdate):
    updated_at: str = Field(default=TIMESTAMP, exclude=False)


class ChangePassword(BaseModel):
    current_password: str
    new_password: str
    confirm_password: str


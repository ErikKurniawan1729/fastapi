from sqlalchemy import (
    Table, 
    Column, 
    Integer, 
    String, 
    DateTime, 
    MetaData, 
    Sequence, 
    TIMESTAMP, 
    BigInteger, 
    Date, 
    Enum, 
    ForeignKey, 
    Text
    )


metadata = MetaData()


tbl_users = Table(
    "tbl_users", 
    metadata,
    Column('id', BigInteger, primary_key=True, autoincrement=True, nullable=False),
    Column('nama_lengkap', String(255), nullable=True),
    Column('username', String(255), nullable=False),
    Column('email', String(255), unique=True, nullable=False),
    Column('telp', String(255), nullable=False),
    Column('password', String(255), nullable=False),
    Column('remember_token', String(255), nullable=True),
    Column('avatar', String(255), nullable=True), 
    Column('nik', BigInteger, nullable=True),
    Column('birth_place', String(50), nullable=True),
    Column('birth_date', Date(), nullable=True),
    Column('gender', Enum('Laki-laki','Perempuan'), nullable=True),
    Column('job', String(255), nullable=True),
    Column('job_location', String(255), nullable=True),
    Column('ktp_province', String(50), nullable=True),
    Column('ktp_city', String(50), nullable=True),
    Column('ktp_district', String(50), nullable=True),
    Column('ktp_postal_code', BigInteger, nullable=True),
    Column('ktp_address', String(255), nullable=True),
    Column('province', String(50), nullable=True),
    Column('city', String(50), nullable=True),
    Column('district', String(50), nullable=True),
    Column('postal_code', BigInteger, nullable=True),
    Column('address', String(255), nullable=True),
    Column('provider_name', String(255), nullable=True),
    Column('email_verified_at', TIMESTAMP, nullable=True),
    Column('biodata_id', BigInteger, ForeignKey(column='tbl_biodata.id', name='fk_tbl_users_tbl_biodata', ondelete='cascade', onupdate='cascade'), unique=True, nullable=True),
    Column('is_active', String(1), nullable=False),
    Column('created_at', TIMESTAMP, nullable=True),
    Column('updated_at', TIMESTAMP, nullable=True)
)


tbl_reset_codes = Table(
    "tbl_reset_codes", 
    metadata, 
    Column("id", Integer, primary_key=True, autoincrement=True, nullable=False), 
    Column("email", String(255), nullable=True), 
    Column("reset_code", String(255), nullable=True), 
    Column("is_active", String(1), nullable=True), 
    Column("expired_at", TIMESTAMP, nullable=True)
)


tbl_blacklist_tokens = Table(
    "tbl_blacklist_tokens", 
    metadata, 
    Column("token", String(255), unique=True, nullable=False), 
    Column("email", String(255), nullable=True), 
    Column("logout_at", TIMESTAMP, nullable=True) 
)


tbl_biodata = Table(
    'tbl_biodata', 
    metadata, 
    Column('id', BigInteger, primary_key=True, autoincrement=True, nullable=False),
    Column('user_id', BigInteger, ForeignKey(column='tbl_users.id', name='fk_tbl_biodata_tbl_users', ondelete='cascade', onupdate='cascade'), unique=True, nullable=True), 
    Column('nik', BigInteger, nullable=True),
    Column('nama_lengkap', String(255), nullable=True),
    Column('telp', String(255), nullable=True),
    Column('gender', Enum('Laki-laki','Perempuan'), nullable=True),
    Column('birth_place', String(50), nullable=True),
    Column('birth_date', Date(), nullable=True),
    Column('ktp_province', String(50), nullable=True),
    Column('ktp_city', String(50), nullable=True),
    Column('ktp_district', String(50), nullable=True),
    Column('ktp_postal_code', BigInteger, nullable=True),
    Column('ktp_address', String(255), nullable=True),
    Column('province', String(50), nullable=True),
    Column('city', String(50), nullable=True),
    Column('district', String(50), nullable=True),
    Column('postal_code', BigInteger, nullable=True),
    Column('address', String(255), nullable=True),
    Column('job', String(255), nullable=True),
    Column('job_location', String(255), nullable=True),
    Column('created_at', TIMESTAMP, nullable=True),
    Column('updated_at', TIMESTAMP, nullable=True)
)


tbl_surveyCategories = Table(
    'tbl_surveyCategories', 
    metadata, 
    Column('id', BigInteger, primary_key=True, autoincrement=True, nullable=False),
    Column('category', Enum('Customers', 'Education', 'Helathcare', 'Employee', 'Market Research'), nullable=False), 
    Column('description', Text, nullable=False), 
    Column('created_at', TIMESTAMP, nullable=True), 
    Column('updated_at', TIMESTAMP, nullable=True)
)


tbl_surveys = Table(
    'tbl_surveys', 
    metadata, 
    Column('id', BigInteger, primary_key=True, autoincrement=True, nullable=False),
    Column('surveycategory_id', BigInteger, ForeignKey(column='tbl_surveyCategories.id', name='fk_tbl_surveys_tbl_surveycategories', ondelete='cascade', onupdate='cascade'), unique=True, nullable=False),
    Column('title', String(255), nullable=True),
    Column('author', String(255), nullable=True),
    Column('description', String(255), nullable=True),
    Column('survey_point', BigInteger, nullable=True),
    Column('status', String(255), nullable=True),
    Column('expired_at', TIMESTAMP, nullable=True),
    Column('deleted_at', TIMESTAMP, nullable=True),
    Column('created_at', TIMESTAMP, nullable=True),
    Column('updated_at', TIMESTAMP, nullable=True)
)


tbl_questions = Table(
    'tbl_questions', 
    metadata, 
    Column('id', BigInteger, primary_key=True, autoincrement=True, nullable=False),
    Column('survey_id', BigInteger, ForeignKey(column='tbl_surveys.id', name='fk_tbl_questions_tbl_surveys', ondelete='cascade', onupdate='cascade'), unique=True, nullable=False),
    Column('question', String(255), nullable=True),
    Column('deleted_at', TIMESTAMP, nullable=True),
    Column('created_at', TIMESTAMP, nullable=True),
    Column('updated_at', TIMESTAMP, nullable=True)
)


tbl_answers = Table(
    'tbl_answers', 
    metadata, 
    Column('id', BigInteger, primary_key=True, autoincrement=True, nullable=False),
    Column('user_id', BigInteger, ForeignKey(column='tbl_users.id', name='fk_tbl_answers_tbl_users', ondelete='cascade', onupdate='cascade'), unique=True, nullable=False),
    Column('question_id', BigInteger, ForeignKey(column='tbl_questions.id', name='fk_tbl_answers_tbl_questions', ondelete='cascade', onupdate='cascade'), nullable=False),
    Column('answer', Text, nullable=True),
    Column('created_at', TIMESTAMP, nullable=True),
    Column('updated_at', TIMESTAMP, nullable=True)
)


tbl_users_surveys = Table(
    'tbl_users_surveys', 
    metadata, 
    Column('id', BigInteger, primary_key=True, autoincrement=True, nullable=False),
    Column('user_id', BigInteger, ForeignKey(column='tbl_users.id', name='fk_tbl_users_surveys_tbl_users', ondelete='cascade', onupdate='cascade'), nullable=False),
    Column('survey_id', BigInteger, ForeignKey(column='tbl_surveys.id', name='fk_tbl_users_surveys_tbl_surveys', ondelete='cascade', onupdate='cascade'), nullable=False),
    Column('created_at', TIMESTAMP, nullable=True),
    Column('updated_at', TIMESTAMP, nullable=True)
)
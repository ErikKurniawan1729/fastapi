


class ResponseException(Exception):
    def __init__(self, status_code: str, status: str, detail: str, data: list):
        self.status_code = status_code
        self.status = status
        self.detail = detail
        self.data = data
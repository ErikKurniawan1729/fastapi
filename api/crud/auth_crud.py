from api.utils.database_util import database
from api.schemas import auth_schema as schema

# Find exist email
def find_exist_user(email: str):
    query = "SELECT * FROM tbl_users WHERE is_active='1' AND email=:email"
    return database.fetch_one(query, values={
        "email": email
        }
    )


# Find exist telp
def find_exist_telp(telp: str):
    query = "SELECT * FROM tbl_users WHERE is_active='1' AND telp=:telp"
    return database.fetch_one(query, values={
        "telp": telp
        }
    )


def find_black_list_token(token: str):
    query = "SELECT * FROM tbl_blacklist_tokens WHERE token=:token"
    return database.fetch_one(query, values={
        "token": token
        }
    )


def save_user(user: schema.UserCreate):
    query = "START TRANSACTION; SET FOREIGN_KEY_CHECKS=0; INSERT INTO tbl_users(id, username, email, telp, password, is_active, created_at, updated_at) VALUES (id, :username, :email, :telp, :password, '1', current_timestamp, current_timestamp); COMMIT;"
    return database.execute(query, values={
        "username": user.username, 
        "email": user.email, 
        "telp": user.telp, 
        "password": user.password
        }
    )

def create_reset_code(email: str, reset_code: str):
    query = "INSERT INTO tbl_reset_codes VALUES (id, :email, :reset_code, '1', current_timestamp)"
    return database.execute(query, values={
        "email": email, 
        "reset_code": reset_code
        }
    )

def check_reset_password_token(reset_password_token: str):
    query = "SELECT * FROM tbl_reset_codes WHERE is_active='1' AND reset_code=:reset_password_token AND expired_at >= expired_at - INTERVAL 10 minute"
    return database.fetch_one(query, values={
        "reset_password_token": reset_password_token
        }
    )

def reset_password(new_hashed_password: str, email: str):
    query = "UPDATE tbl_users SET password=:password WHERE email=:email"
    return database.execute(query, values={
        "password": new_hashed_password, 
        "email": email
        }
    )

def disable_reset_code(reset_password_token: str, email: str):
    query = "UPDATE tbl_reset_codes SET is_active='0' WHERE is_active='1' AND reset_code=:reset_code AND email=:email"
    return database.execute(query, values={
        "reset_code": reset_password_token, 
        "email": email
        }
    )


from api.utils.database_util import database
from api.schemas import auth_schema
from api.schemas import user_schema


def update_user(request: user_schema.UserUpdateTimestamp, current_user: auth_schema.UserList):
    query = "UPDATE tbl_users SET nik=:nik, nama_lengkap=:nama_lengkap, email=:email, gender=:gender, birth_place=:birth_place, birth_date=:birth_date, job=:job, job_location=:job_location, province=:province, city=:city, district=:district, postal_code=:postal_code, address=:address, updated_at=current_timestamp WHERE email=:email"
    return database.execute(query, values={
            "nik" : request.nik,
            "nama_lengkap" : request.nama_lengkap,
            "email" : request.email,
            "gender" : request.gender,
            "birth_place" : request.birth_place,
            "birth_date" : request.birth_date,
            "job" : request.job,
            "job_location" : request.job_location,
            "province" : request.province,
            "city" : request.city,
            "district" : request.district,
            "postal_code" : request.postal_code,
            "address" : request.address,
            "email": current_user.email
        }
    )


def deactivate_user(current_user: auth_schema.UserList):
    query = "UPDATE tbl_users SET is_active='0' WHERE email=:email"
    return database.execute(query, values={
            "email": current_user.email
        }
    )

def change_password(change_password_object: user_schema.ChangePassword, current_user: auth_schema.UserList):
    query = "UPDATE tbl_users SET password=:password WHERE is_active='1' AND email=:email"
    return database.execute(query, values={
            "password": change_password_object.new_password, 
            "email": current_user.email
        }
    )


def save_black_list_token(token: str, current_user: auth_schema.UserList):
    query = "INSERT INTO tbl_blacklist_tokens VALUES (:token, :email, current_timestamp)"
    return database.execute(query, values={
            "token": token, 
            "email": current_user.email
        }
    )
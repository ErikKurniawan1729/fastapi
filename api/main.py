from fastapi import FastAPI, Request
from fastapi.responses import JSONResponse
from api.utils.database_util import database
from api.exceptions.custom_exception import ResponseException

from api.routes.auth import (
    forgot_password_routes,
    login_routes,
    register_routes,
    reset_password_routes
)

from api.routes.users import (
    change_password_routes,
    delete_account_routes,
    edit_profile_routes,
    get_profile_images_routes,
    get_profile_routes,
    logout_routes,
    upload_profile_images_routes,
    upload_ktp_routes
)

from api.routes.public import check_email_routes
from api.routes import root_routes

app = FastAPI(
    docs_url='/docs',
    redoc_url='/redocs',
    title='FastAPI (Python)',
    description='FastAPI Framework, high performance, </br> '
    'easy to learn, fast to code, ready for production',
    version='1.0',
    openapi_url='/openapi.json'
)


@app.on_event("startup")
async def startup():
    await database.connect()


@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()


@app.exception_handler(ResponseException)
async def unicorn_exception_handler(request: Request, e: ResponseException):
    return JSONResponse(
        status_code=418,
        content={
            "code": e.status_code,
            "status": e.status,
            "message": e.detail,
            "data": e.data
        }
    )

app.include_router(forgot_password_routes.router, tags=["Auth"])
app.include_router(login_routes.router, tags=["Auth"])
app.include_router(logout_routes.router, tags=["Auth"])
app.include_router(register_routes.router, tags=["Auth"])
app.include_router(reset_password_routes.router, tags=["Auth"])

app.include_router(change_password_routes.router, tags=["Users"])
app.include_router(delete_account_routes.router, tags=["Users"])
app.include_router(edit_profile_routes.router, tags=["Users"])
app.include_router(get_profile_images_routes.router, tags=["Users"])
app.include_router(get_profile_routes.router, tags=["Users"])
app.include_router(upload_profile_images_routes.router, tags=["Users"])
app.include_router(upload_ktp_routes.router, tags=["Users"])

app.include_router(check_email_routes.router, tags=["Public"])

app.include_router(root_routes.router, tags=["Root"])

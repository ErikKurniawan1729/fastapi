from fastapi import APIRouter, Depends
from api.schemas import auth_schema, user_schema
from api.crud import user_crud
from api.utils import jwt_util
from api.routes.users.response_model.custom_response import response_schema
import re
from api.exceptions.custom_exception import ResponseException


router = APIRouter(
    prefix="/api/users"
)


@router.patch("/profile")
async def update_profile(request: user_schema.UserUpdateTimestamp, current_user: auth_schema.UserList = Depends(jwt_util.get_current_user)):
    # Update user info
    await user_crud.update_user(request, current_user)
    # Email harus mengandung @
    if not re.match(r'[^@]+@[^@]+\.[^@]+', request.email):
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Email tidak valid.", 
            data=[]
        )
    return response_schema(
        status_code=200, 
        status="OK", 
        detail="Berhasil menambahkan informasi pribadi.", 
        data=[
            {
                "nik": request.nik,
                "nama_lengkap": request.nama_lengkap,
                "email": request.email,
                "gender": request.gender, 
                "birth_place": request.birth_place,
                "birth_date": request.birth_date,
                "job": request.job,
                "job_location": request.job_location,
                "province": request.province,
                "city": request.city,
                "district": request.district,
                "postal_code": request.postal_code,
                "address": request.address
            }
        ]
    )
from fastapi import APIRouter, Depends, status
from api.schemas import auth_schema
from api.crud import user_crud
from api.utils import jwt_util


router = APIRouter(
    prefix="/api/users"
)




@router.delete("/profile")
async def deactivate_account(current_user: auth_schema.UserList = Depends(jwt_util.get_current_active_user)):
    # delete user
    await user_crud.deactivate_user(current_user)
    return {
        "code": status.HTTP_200_OK, 
        "message": "User account has been deactivated."
    }
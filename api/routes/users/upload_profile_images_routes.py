from fastapi import APIRouter, Depends, File, UploadFile
from api.schemas import auth_schema
from api.utils import jwt_util
import os
from api.routes.users.response_model.custom_response import response_schema


router = APIRouter(
    prefix="/api/users"
)


@router.post("/upload-profile-images")
async def upload_profile_image(file: UploadFile = File(...), current_user: auth_schema.UserList = Depends(jwt_util.get_current_active_user)):
    try:
        cwd = os.getcwd()
        path_image_dir = "resource/user/profile/" + str(current_user.id) + "/"
        full_image_path = os.path.join(cwd, path_image_dir, file.filename)
        
        print(full_image_path)

        # create directory if not exist
        if not os.path.exists(path_image_dir):
            os.mkdir(path_image_dir)

        # rename file to 'profile'
        file_name = full_image_path.replace(file.filename, "profile.png")

        # write file
        with open(file_name, 'wb+') as f:
            f.write(file.file.read())
            f.flush()
            f.close()
        
        return response_schema(
            status_code=200, 
            status="OK", 
            detail="Berhasil mengunggah foto profil.", 
            data=[
                {
                    "username": current_user.username, 
                    "email": current_user.email, 
                    "profile_image": os.path.join(path_image_dir, "profile.png")
                }
            ]
        )
    except Exception as e:
        print(e)

from fastapi import APIRouter, Depends, status, HTTPException, File, UploadFile
from api.schemas import auth_schema, user_schema
from api.crud import auth_crud, user_crud
from api.utils import hashing_util, jwt_util
import os
from PIL import Image

router = APIRouter(
    prefix="/api/users"
)



@router.patch("/change-password")
async def change_password(change_password_object: user_schema.ChangePassword, current_user: auth_schema.UserList = Depends(jwt_util.get_current_active_user)):
    # check user exist
    result = await auth_crud.find_exist_user(current_user.email)
    if not result:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail="User not found.")
    
    # verify current password
    user = auth_schema.UserCreate(**result)
    valid = hashing_util.verify_password(change_password_object.current_password, user.password)
    if not valid: 
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="Current password is not match.")

    # check new password and confirm password
    if change_password_object.new_password != change_password_object.confirm_password:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED, detail="New password is not match.")

    # change password
    change_password_object.new_password = hashing_util.hash_password(change_password_object.new_password)
    await user_crud.change_password(change_password_object, current_user)
    return {
        "code": status.HTTP_200_OK, 
        "message": "Password has been changed successfully."
    }
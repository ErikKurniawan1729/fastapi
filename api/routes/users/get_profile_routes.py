from fastapi import APIRouter, Depends
from api.schemas import auth_schema
from api.utils import jwt_util


router = APIRouter(
    prefix="/api/users"
)


@router.get("/profile")
async def get_user_profile(current_user: auth_schema.UserList = Depends(jwt_util.get_current_active_user)):
    return current_user
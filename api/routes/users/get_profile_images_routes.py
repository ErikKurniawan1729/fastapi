from fastapi import APIRouter, Depends
from api.schemas import auth_schema
from api.utils import jwt_util
import os
from PIL import Image

router = APIRouter(
    prefix="/api/users"
)


@router.get("/upload-profile-images")
async def get_profile_image(current_user: auth_schema.UserList = Depends(jwt_util.get_current_active_user)):
    try:
        cwd = os.getcwd()
        path_image_dir = "upload-images/user/profile/" + str(current_user.id) + "/"
        full_image_path = os.path.join(cwd, path_image_dir, "profile.png")
        
        # check if exist
        if os.path.exists(full_image_path):
            # resize profile image to 400x400
            image = Image.open(full_image_path)
            image.thumbnail((400, 400), Image.ANTIALIAS)
        
        return {
            "profile_image": os.path.join(path_image_dir, "profile_400x400.png")
        }
    except Exception as e:
        print(e)




from fastapi import APIRouter, Depends
from api.schemas import auth_schema
from api.crud import user_crud
from api.utils import jwt_util
from api.routes.users.response_model.custom_response import response_schema

router = APIRouter(
    prefix="/api/users"
)

@router.get("/logout")
async def logout(token: str = Depends(jwt_util.get_token_user), current_user: auth_schema.UserList = Depends(jwt_util.get_current_active_user)):
    # Save token of user to table blacklist
    await user_crud.save_black_list_token(token, current_user)
    return response_schema(
        status_code=200, 
        status="OK", 
        detail="User logged out successfully.", 
        data=[]
    )
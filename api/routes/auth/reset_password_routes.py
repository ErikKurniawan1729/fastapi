from fastapi import APIRouter
from api.schemas import auth_schema as schema
from api.crud import auth_crud as crud
from api.utils import hashing_util
from api.exceptions.custom_exception import ResponseException
from api.routes.auth.response_model.custom_response import response_schema

router = APIRouter(
    prefix="/api/auth"
)



@router.patch("/reset-password")
async def reset_password(request: schema.ResetPassword):
    # Check valid reset password token
    reset_token = await crud.check_reset_password_token(request.reset_password_token)
    if not reset_token:
        raise ResponseException(
            status_code=404, 
            status="NOT_FOUND", 
            detail="Reset password token has expired, please request a new one.", 
            data=[]
        )

    # Check both new & confirm password are matched
    if request.new_password != request.confirm_password:
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Kata sandi tidak cocok.", 
            data=[]
        )

    # Reset new password
    forgot_password_object = schema.ForgotPassword(**reset_token)
    new_hashed_password = hashing_util.hash_password(request.new_password)
    await crud.reset_password(new_hashed_password, forgot_password_object.email)

    # Disable reset code (already used)
    await crud.disable_reset_code(request.reset_password_token, forgot_password_object.email)
    return response_schema(
        status_code=200, 
        status="OK", 
        detail="Password has been reset successfully.", 
        data=[]
    )
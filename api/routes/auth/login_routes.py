from fastapi import APIRouter, Depends
from api.schemas import auth_schema as schema
from api.crud import auth_crud as crud
from api.utils import constant_util, hashing_util, jwt_util
from fastapi.security import OAuth2PasswordRequestForm
from api.exceptions.custom_exception import ResponseException
import re
from api.routes.auth.response_model.custom_response import response_schema

router = APIRouter(
    prefix="/api/auth"
)


@router.post("/login")
async def login(form_data: OAuth2PasswordRequestForm = Depends()):
    # Cek user terdaftar
    result = await crud.find_exist_user(form_data.username)
    if not result:
        raise ResponseException(
            status_code=404, 
            status="NOT_FOUND", 
            detail="User tidak terdaftar.", 
            data=[]
        )
    
    # Email harus mengandung @
    if not re.match(r'[^@]+@[^@]+\.[^@]+', form_data.username):
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Email tidak valid.", 
            data=[]
        )
    
    # Verifikasi password
    user = schema.UserCreate(**result)
    verified_password = hashing_util.verify_password(form_data.password, user.password)
    if not verified_password:
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Kata sandi tidak cocok.", 
            data=[]
        )
    
    # Create TOKEN
    access_token_expires = jwt_util.timedelta(minutes=constant_util.ACCESS_TOKEN_EXPIRE_MINUTE)
    access_token = await jwt_util.create_access_token(
        data={"sub": form_data.username}, 
        expires_delta=access_token_expires
    )   
    
    return response_schema(
        status_code=200, 
        status="OK", 
        detail="Login berhasil.", 
        data=[
            {
                "access_token": access_token, 
                "token_type": "bearer", 
                "user_info": {
                    "email": user.email, 
                    "username": user.username
                }
            }
        ]
    )
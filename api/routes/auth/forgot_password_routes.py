from fastapi import APIRouter
from api.schemas import auth_schema as schema
from api.crud import auth_crud as crud
from api.utils import otp_util
from api.exceptions.custom_exception import ResponseException
from api.routes.auth.response_model.custom_response import response_schema

router = APIRouter(
    prefix="/api/auth"
)


@router.post("/forgot-password")
async def forgot_password(request: schema.ForgotPassword):
    # Check user terdaftar
    result = await crud.find_exist_user(request.email)
    if not result:
        raise ResponseException(
            status_code=404, 
            status="NOT_FOUND", 
            detail="User tidak terdaftar.", 
            data=[]
        )
    
    # Create OTP and store into database
    reset_code = otp_util.random(6)
    await crud.create_reset_code(request.email, reset_code)

    # Sending email
    subject = "SurveyAsia"
    recipient = [request.email]
    message = """
        <!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Reset Password</title>
        </head>
        <body>
            <div style="font-family: Helvetica,Arial,sans-serif;min-width:1000px;overflow:auto;line-height:2">
                <div style="margin:50px auto;width:70%;padding:20px 0">
                    <div style="border-bottom:1px solid #eee">
                    <a href="" style="font-size:1.4em;color: #00466a;text-decoration:none;font-weight:600">SurveyAsia</a>
                    </div>
                    <p style="font-size:1.1em">Hi {0:},</p>
                    <p>Please use the verification code below to reset your password. OTP is valid for 5 minutes</p>
                    <h2 style="background: #00466a;margin: 0 auto;width: max-content;padding: 0 10px;color: #fff;border-radius: 4px;">{1:}</h2>
                    <p style="font-size:0.9em;">Regards,<br />SurveyAsia</p>
                    <hr style="border:none;border-top:1px solid #eee" />
                    <div style="float:right;padding:8px 0;color:#aaa;font-size:0.8em;line-height:1;font-weight:300">
                    </div>
                </div>
            </div>
        </body>
        </html>
            """.format(request.email, reset_code)
    
    return response_schema(
        status_code=200, 
        status="OK", 
        detail="We've send an email with instruction to reset your password.", 
        data=[
            {
                "reset_code": reset_code
            }
        ]
    )
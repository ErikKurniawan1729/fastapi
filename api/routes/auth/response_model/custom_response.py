from fastapi.responses import JSONResponse

def response_schema(status_code: int, status: str, detail: str, data: list):
    return JSONResponse(
        status_code=status_code, 
        content={
            "code": status_code, 
            "status": status, 
            "message": detail, 
            "data": data
        }
    )
from fastapi import APIRouter
from api.schemas import auth_schema as schema
from api.crud import auth_crud as crud
from api.utils import hashing_util
import re
from api.exceptions.custom_exception import ResponseException
from api.routes.auth.response_model.custom_response import response_schema


router = APIRouter(
    prefix="/api/auth"
)



@router.post("/register", response_model=schema.UserList)
async def register(user: schema.UserCreate):
    # Cek email user
    result = await crud.find_exist_user(user.email)
    if result:
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Email telah digunakan.", 
            data=[]
        )

    # Cek nomor ponsel user
    telp = await crud.find_exist_telp(user.telp)
    if telp:
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Nomor Ponsel telah digunakan.", 
            data=[]
        )
    
    # Username minimal 8 karakter
    if len(user.username) < 8:
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Username minimal 8 karakter.", 
            data=[]
        )
    
    # Email harus mengandung @
    if not re.match(r'[^@]+@[^@]+\.[^@]+', user.email):
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Email tidak valid.", 
            data=[]
        )
    
    # Nomor ponsel harus 8 sampai 13 digit
    if len(user.telp) < 8 or len(user.telp) > 13:
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Nomor ponsel harus 8 sampai 13 digit.", 
            data=[]
        )
    
    # Password dan konfirmasi password harus sama
    if user.password != user.confirm_password:
        raise ResponseException(
            status_code=400, 
            status="BAD_REQUEST", 
            detail="Kata sandi tidak cocok.", 
            data=[]
        )

    # Create new user
    user.password = hashing_util.hash_password(user.password)
    await crud.save_user(user)
    return response_schema(
        status_code=201, 
        status="CREATED", 
        detail="User berhasil didaftarkan.", 
        data=[
            { 
                "username": user.username, 
                "email": user.email, 
                "telp": user.telp
            }
        ]
    )

from fastapi import APIRouter
from api.crud import auth_crud as crud
from api.exceptions.custom_exception import ResponseException
from api.routes.auth.response_model.custom_response import response_schema
from api.schemas import public_schema

router = APIRouter(
    prefix="/api/public"
)


@router.post("/email-check", response_model=public_schema.EmailCheckResponse)
async def email_check(user: public_schema.EmailCheck):
    # Cek email user
    result = await crud.find_exist_user(user.email)
    if result:
        raise ResponseException(
            status_code=200, 
            status="OK", 
            detail="Email telah digunakan.", 
            data=[
                {
                    "email": user.email
                }
            ]
        )
    if not result:
        return response_schema(
        status_code=404, 
        status="NOT_FOUND", 
        detail="Email belum digunakan.", 
        data=[]
    )
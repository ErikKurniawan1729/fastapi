from fastapi import APIRouter



router = APIRouter()

@router.get("/")
async def root():
    return {
        "message": "Welcome to FastAPI SurveyAsia", 
        "authors": "Data Engineer SurveyAsia", 
        "api_documentation": "https://documenter.getpostman.com/view/23318264/2s8YzMXQKR"
    }